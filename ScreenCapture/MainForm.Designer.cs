﻿namespace ScreenCapture
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shareBtn = new System.Windows.Forms.Button();
            this.seeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // shareBtn
            // 
            this.shareBtn.Location = new System.Drawing.Point(12, 12);
            this.shareBtn.Name = "shareBtn";
            this.shareBtn.Size = new System.Drawing.Size(75, 23);
            this.shareBtn.TabIndex = 0;
            this.shareBtn.Text = "Share";
            this.shareBtn.UseVisualStyleBackColor = true;
            this.shareBtn.Click += new System.EventHandler(this.shareBtn_Click);
            // 
            // seeBtn
            // 
            this.seeBtn.Location = new System.Drawing.Point(12, 41);
            this.seeBtn.Name = "seeBtn";
            this.seeBtn.Size = new System.Drawing.Size(75, 23);
            this.seeBtn.TabIndex = 1;
            this.seeBtn.Text = "See";
            this.seeBtn.UseVisualStyleBackColor = true;
            this.seeBtn.Click += new System.EventHandler(this.seeBtn_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(98, 69);
            this.Controls.Add(this.seeBtn);
            this.Controls.Add(this.shareBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "ShareZone";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button shareBtn;
        private System.Windows.Forms.Button seeBtn;
    }
}
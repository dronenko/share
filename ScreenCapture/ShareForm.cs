﻿using System;
using System.Windows.Forms;
using ScreenCapture.Views;
using ScreenCapture.Presenters;
using System.Drawing;
using System.Drawing.Imaging;
using System.ComponentModel;
using System.IO;

namespace ScreenCapture
{
    public partial class ShareForm : Form, IShare
    {       
        private BroadcastActiv activeForm;
        private BroadcastNoActiv noActiveForm;
        private ServerPresenters presenters;
        private bool start = false;
        private bool sizeForm = false;
        private TimerClass timer;
        private Bitmap printscreen;
        private Bitmap partScreen;
        private Graphics graphics;
        private Rectangle rectangle;
        private Icon cursor;
        private byte[] data;
        private int countClient;
      
        public string IAddressIP
        {
            set
            {
                activeForm.Text = value;
            }
        }       

        public byte[] ISendData
        {
            get
            {
                return data;
            }
        }

        public int IListClient
        {
            set
            {
                countClient = value;
            }
        }

        public ShareForm()
        {
            InitializeComponent();              
        }

        // Присвоюємо параметри для прямокутника
        public void Parameters()
        {
            rectangle.X = activeForm.Location.X + 8;
            rectangle.Y = activeForm.Location.Y + 31;
            rectangle.Width = activeForm.Width - 16; 
            rectangle.Height = activeForm.Height - 39;
        }

        // Передача даних клієнту
        public void CaptureScreen(object sender, EventArgs e)
        {
            try
            {
                using (printscreen = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height))
                {

                    using (graphics = Graphics.FromImage(printscreen as Image))
                    {                      
                            graphics.CopyFromScreen(Point.Empty, Point.Empty, printscreen.Size);
                            graphics.DrawIcon(cursor, Cursor.Position.X, Cursor.Position.Y);                        
                    }
                    using (partScreen = printscreen.Clone(rectangle, PixelFormat.Format16bppRgb555))
                    {
                        presenters.NumberClient();
                        if (countClient != 0)
                        {
                            TypeConverter converter = TypeDescriptor.GetConverter(partScreen.GetType());
                            data = (byte[])converter.ConvertTo(partScreen, typeof(byte[]));
                            presenters.PresentersCaptureScreen();
                        }
                    }
                }
            }
            catch (OutOfMemoryException)
            {
                StopServer();
                start = false;
                MessageBox.Show("You have selected an invalid area", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // Отримання параметрів для другої форми яка не активна
        private void FormSize()
        {
            noActiveForm = new BroadcastNoActiv();
            noActiveForm.Show();
            noActiveForm.Text = activeForm.Text;
            noActiveForm.Left = activeForm.Location.X;
            noActiveForm.Top = activeForm.Location.Y;
            noActiveForm.Width = activeForm.Width;
            noActiveForm.Height = activeForm.Height;
            activeForm.Hide();
        }
        
        // Запуск форми
        private void MainForm_Load(object sender, EventArgs e)
        {
            activeForm = new BroadcastActiv();
            activeForm.Show();
            ServerPresenters pres = new ServerPresenters(this);
            pres.ILocalAddress();
        }

        // Закриття форми
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (activeForm.ShowIcon)
            {
                activeForm.Close();
            }
        }    

        // Передача даних по таймеру
        public void StartBroad()
        {
            cursor = new Icon("cursor.ico");
            timer = new TimerClass();
            timer.Timers(CaptureScreen, 100);           
        }

        // Зупинка сервера, таймера та закриття форм
        private void StopServer()
        {
            presenters.IStopServer();
            timer.TimersStop();
            timer.Dispose();
            cursor.Dispose();
            noActiveForm.Close();
            this.Close();
            start = false;
            presenters.Dispose();
        }

        // Подія запуску сервера, таймера та передачі даних
        private void startBtn_Click_1(object sender, EventArgs e)
        {
            if (!start)
            {
                presenters = new ServerPresenters(this);
                presenters.StartServer();
                Parameters();
                FormSize();
                StartBroad();
                start = true;
                startBtn.Text = "Stop";
                sizeBtn.Enabled = true;
            }
            else
            {
                StopServer();
            }
        }

        // Зміна розміру області трансляції
        private void sizeBtn_Click_1(object sender, EventArgs e)
        {
            if (!sizeForm)
            {
                activeForm.Show();
                noActiveForm.Close();
                sizeForm = true;
                sizeBtn.Text = "Ok";
            }
            else
            {
                FormSize();
                Parameters();
                sizeForm = false;
                sizeBtn.Text = "SizeWindows";
            }
        }
    }
}

﻿using System.Drawing;
using System.Windows.Forms;

namespace ScreenCapture
{
    public partial class BroadcastActiv : Form
    {
        public BroadcastActiv()
        {
            this.FormBorderStyle = FormBorderStyle.None;
            this.AllowTransparency = true;
            this.BackColor = Color.AliceBlue;
            this.TransparencyKey = this.BackColor;
            InitializeComponent();

        }    

    }  
  
}

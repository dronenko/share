﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScreenCapture.Model
{
    public class NewEventArgs
    {
        public NewEventArgs(byte[] getBytesEvent)
        {
            Bytes = getBytesEvent;
        }
        public byte[] Bytes { get; private set; }
    }

    public delegate void MyEventHandler(object sender, NewEventArgs e);

    class SeeModel : IDisposable
    {
        private TcpClient client;
        private IPEndPoint ipEnd;
        private NetworkStream streamingData;
        private CancellationTokenSource cancelTaskConnect;
        private CancellationTokenSource cancelTaskOutput;
        private string ipAddress;
        public byte[] bytesFrom;

        public event MyEventHandler outputEvent;

        public bool ClientConnected { get; set; }
        public bool ClientFailed { get; set; }
        public string GetHostAddress { get; set; }
        public int LengthByte { get; set; }

        public void StreamConnect()
        {
            cancelTaskConnect = new CancellationTokenSource();
            Task taskConnect = Task.Run(() => СonnectToServer(cancelTaskConnect.Token), cancelTaskConnect.Token);
        }       

        public void СonnectToServer(CancellationToken cancelTask)
        {
            ClientConnected = false;
            ClientFailed = false;
            try
            {
                IPAddress[] localIP = Dns.GetHostAddresses(GetHostAddress);
                foreach (IPAddress address in localIP)
                {
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = address.ToString();
                    }
                }

                client = new TcpClient();
                ipEnd = new IPEndPoint(IPAddress.Parse(ipAddress), 8085);

                client.Connect(ipEnd);

                if (client.Connected)
                {
                    streamingData = client.GetStream();

                    ClientConnected = true;
                }

                cancelTask.ThrowIfCancellationRequested();

            }
            catch (SocketException)
            {
                ClientFailed = true;
            }
        }

        public void OutputStream(CancellationToken cancelTask)
        {
            try
            {               
                LengthByte = -1;
                byte[] lengthBytes = new byte[1];
                string strCheck;
                string strLengthBytes;

                while (LengthByte != 0)
                {
                    strLengthBytes = "";
                    strCheck = "";

                    while (strCheck != ".") {
                        strLengthBytes += strCheck;
                        streamingData.Read(lengthBytes, 0, 1);
                        strCheck = Encoding.ASCII.GetString(lengthBytes);                        
                    }

                    if (strLengthBytes != "")
                    {
                        byte[] length = new byte[int.Parse(strLengthBytes)];

                        streamingData.Read(length, 0, length.Length);
                        string strLengthMasiv = Encoding.ASCII.GetString(length);
                        int iter = strLengthMasiv.IndexOf("@End");

                        if (iter > 0)
                        {
                            strLengthMasiv = strLengthMasiv.Remove(iter, strLengthMasiv.Length - iter);
                            bytesFrom = new byte[int.Parse(strLengthMasiv)];
                            LengthByte = streamingData.Read(bytesFrom, 0, bytesFrom.Length);
                            if (outputEvent != null)
                                outputEvent(this, new NewEventArgs(bytesFrom));
                        }
                    }
                    else
                    {
                        LengthByte = 0;
                    }

                    streamingData.Flush();
                    cancelTask.ThrowIfCancellationRequested();         
                }                
            }
            catch (IOException) { }
            catch (ObjectDisposedException) { } 
        }

        public void StreamOutput()
        {
            cancelTaskOutput = new CancellationTokenSource();
            Task taskClient = Task.Run(() => OutputStream(cancelTaskOutput.Token), cancelTaskOutput.Token);            
        }
        
        public void StopClient()
        {
            cancelTaskOutput.Cancel();
            streamingData.Close();
            client.Close();                                    
        }        

        public void StopThreadclt()
        {
            cancelTaskConnect.Cancel();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                client.Close();
                cancelTaskConnect.Dispose();
                cancelTaskOutput.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

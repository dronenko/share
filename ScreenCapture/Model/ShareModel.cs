﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ScreenCapture.Model
{
    public class ShareModel : IDisposable
    {
        private TcpClient client;
        private TcpListener listener;
        private CancellationTokenSource cancelTaskStartServer;
        private List<ClientConnection> listClients = new List<ClientConnection>();
        private byte[] secondImg = { };
        

        public byte[] SendData { get; set; }
        public int ListClient { get; set; }

        public void StartServer(CancellationToken ct)
        {
            try
            {
                listener = new TcpListener(IPAddress.Any, 8085);
                listener.Start();

                while (true)
                {
                    client = listener.AcceptTcpClient();
                    ClientConnection newClient = new ClientConnection(client);
                    listClients.Add(newClient);
                    ListClient = listClients.Count;
                    ct.ThrowIfCancellationRequested();
                }                
            }
            catch (SocketException){ };
        }

        public string LocalIpAddress()
        {
            string addressIP = "";
            IPAddress[] localIP = Dns.GetHostAddresses(Dns.GetHostName());
            foreach (IPAddress address in localIP)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                {
                   addressIP = address.ToString();
                }
            }
            return addressIP;
        }

        public void StreamStartServer()
        {
            cancelTaskStartServer = new CancellationTokenSource();
            Task taskStartServer = Task.Run(() => StartServer(cancelTaskStartServer.Token), cancelTaskStartServer.Token);            
        }       

        public void CaptureScreen()
        {
            if (secondImg.Length != SendData.Length)
            {
                foreach (ClientConnection transferToClient in listClients.ToArray())
                {
                    try
                    {
                        byte[] lengthImg = Encoding.ASCII.GetBytes(SendData.Length.ToString() + "@End");
                        byte[] lengthMas = Encoding.ASCII.GetBytes(lengthImg.Length.ToString() + ".");
                        transferToClient.Send(lengthMas);
                        transferToClient.Send(lengthImg);
                        Thread.Sleep(1);
                        transferToClient.Send(SendData);
                    }
                    catch (System.IO.IOException)
                    {
                        transferToClient.Stop();
                        listClients.Remove(transferToClient);
                    }
                }
            }
            secondImg = SendData;                         
        }            

        public void StopServer()
        {
            {
                if (client != null)
                {

                    foreach (ClientConnection stopClient in listClients)
                    {
                        stopClient.Stop();
                    }
                    listClients.Clear();
                }
                listener.Stop();
                cancelTaskStartServer.Cancel();
                cancelTaskStartServer.Dispose();            
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                cancelTaskStartServer.Dispose();                
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

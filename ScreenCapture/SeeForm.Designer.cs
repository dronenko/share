﻿namespace ScreenCapture
{
    partial class SeeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {                
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inputAddress = new System.Windows.Forms.TextBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.stopBtn = new System.Windows.Forms.Button();
            this.addressText = new System.Windows.Forms.Label();
            this.connectText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // inputAddress
            // 
            this.inputAddress.Location = new System.Drawing.Point(18, 34);
            this.inputAddress.Name = "inputAddress";
            this.inputAddress.Size = new System.Drawing.Size(198, 20);
            this.inputAddress.TabIndex = 0;
            this.inputAddress.Text = "192.168.1.103";
            // 
            // startBtn
            // 
            this.startBtn.Location = new System.Drawing.Point(18, 69);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(75, 23);
            this.startBtn.TabIndex = 1;
            this.startBtn.Text = "Start";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.Location = new System.Drawing.Point(141, 69);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(75, 23);
            this.stopBtn.TabIndex = 2;
            this.stopBtn.Text = "Stop";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Visible = false;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // addressText
            // 
            this.addressText.AutoSize = true;
            this.addressText.Location = new System.Drawing.Point(24, 18);
            this.addressText.Name = "addressText";
            this.addressText.Size = new System.Drawing.Size(81, 13);
            this.addressText.TabIndex = 3;
            this.addressText.Text = "Sourse Address";
            // 
            // connectText
            // 
            this.connectText.AutoSize = true;
            this.connectText.Location = new System.Drawing.Point(23, 74);
            this.connectText.Name = "connectText";
            this.connectText.Size = new System.Drawing.Size(70, 13);
            this.connectText.TabIndex = 4;
            this.connectText.Text = "Connecting...";
            this.connectText.Visible = false;
            // 
            // SeeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(232, 103);
            this.Controls.Add(this.connectText);
            this.Controls.Add(this.addressText);
            this.Controls.Add(this.stopBtn);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.inputAddress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SeeForm";
            this.Text = "ShareZone";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox inputAddress;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.Label addressText;
        private System.Windows.Forms.Label connectText;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScreenCapture.Views
{
    public interface IShare
    {      
        string IAddressIP { set; }
        byte[] ISendData { get; }
        int IListClient { set; }
    }
}

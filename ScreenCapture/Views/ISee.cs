﻿namespace ScreenCapture.Views
{
    interface ISee
    {
        string IGetHostAddress { get; }
        bool IClientConnected { set; }
        bool IClientFailed { set; }
        int ILengthByte { set; }
        void OutputData(byte[] getBytesData);        
    }
}

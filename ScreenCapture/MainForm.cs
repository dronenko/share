﻿using System;
using System.Windows.Forms;

namespace ScreenCapture
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void shareBtn_Click(object sender, EventArgs e)
        {
            ShareForm shareForm = new ShareForm();
            shareForm.Show();
        }

        private void seeBtn_Click(object sender, EventArgs e)
        {
            SeeForm seeForm = new SeeForm();
            seeForm.Show();
        }
    }
}

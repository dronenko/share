﻿namespace ScreenCapture
{
    partial class Review
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.revisionZone = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.revisionZone)).BeginInit();
            this.SuspendLayout();
            // 
            // revisionZone
            // 
            this.revisionZone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.revisionZone.Location = new System.Drawing.Point(0, 0);
            this.revisionZone.Name = "revisionZone";
            this.revisionZone.Size = new System.Drawing.Size(344, 270);
            this.revisionZone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.revisionZone.TabIndex = 0;
            this.revisionZone.TabStop = false;
            // 
            // Review
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 270);
            this.Controls.Add(this.revisionZone);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "Review";
            this.Text = "ShareZone";
            ((System.ComponentModel.ISupportInitialize)(this.revisionZone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox revisionZone;
    }
}
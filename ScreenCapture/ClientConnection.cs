﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ScreenCapture
{
    public class ClientConnection
    {
        public TcpClient connection;
        private NetworkStream socketStream;

        public ClientConnection(TcpClient connected)
        {
            connection = connected;
            socketStream = connection.GetStream();
        }

        public void Send(byte[] Data)
        {           
            socketStream.Write(Data, 0, Data.Length);
            socketStream.Flush();    
        }

        public void Stop()
        {
           socketStream.Close();
            connection.Close();
        }
    }
}

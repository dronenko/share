﻿using System;


namespace ScreenCapture
{
    class TimerClass :IDisposable
    {
        System.Windows.Forms.Timer MyTimer;

        public void Timers(EventHandler e, int i)
        {
            MyTimer = new System.Windows.Forms.Timer();
            MyTimer.Interval = (i);
            MyTimer.Tick += new EventHandler(e);
            MyTimer.Start();
        }

        public void TimersStop()
        {
            MyTimer.Stop();
        }

        protected virtual void ShareForm(bool disposing)
        {
            if (disposing)
            {
                MyTimer.Dispose();           
            }
        }

        public void Dispose()
        {
            ShareForm(true);
            GC.SuppressFinalize(this);
        }
    }
}

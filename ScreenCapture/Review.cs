﻿using System.Drawing;
using System.Windows.Forms;

namespace ScreenCapture
{
    public partial class Review : Form
    {
        public Review()
        {
            InitializeComponent();
        }

        public void SetPicture(Bitmap bmp)
        {            
            revisionZone.Image = new Bitmap(bmp);
        }
    }
}

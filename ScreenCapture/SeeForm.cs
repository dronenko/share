﻿using System;
using System.Drawing;
using System.Windows.Forms;
using ScreenCapture.Views;
using ScreenCapture.Presenters;
using System.IO;
using System.Text;

namespace ScreenCapture
{
    public partial class SeeForm : Form, ISee
    {         
        private Review reviewForm;
        private Bitmap finalBitmap;
        private TimerClass revisionConnect;
        private TimerClass revisionFail;
        private SeePresenters seePresenters;
        private MemoryStream buffer;
        private int lengthByte;
        private bool clientConnected;
        private bool clientFailed;
        private byte[] byteReceiving;        

        public string IGetHostAddress
        {
            get
            {
                return inputAddress.Text;
            }
        }

        public bool IClientConnected
        {
            set
            {
                clientConnected = value;
            }
        }

        public bool IClientFailed
        {
            set
            {
                clientFailed = value;
            }
        }     

        public int ILengthByte
        {
            set
            {
                lengthByte = value;
            }
        }

        public byte[] IBytesFrom
        {
            set
            {
                byteReceiving = value;
            }
        }

        public SeeForm()
        {
            InitializeComponent();
        }       

        public void OutputData(byte[] getByteData)
        {
            using (buffer = new MemoryStream(getByteData))
            {
                finalBitmap = new Bitmap(Image.FromStream(buffer));

                if (finalBitmap != null)
                {
                    reviewForm.SetPicture(finalBitmap);
                }
            }          
        }

        private void StartOutput(object sender, EventArgs e)
        {
            seePresenters.ConnFaild();
            if (clientConnected)
            {               
                revisionFail.Timers(FailedConnection, 8000);

                reviewForm = new Review();
                reviewForm.Show();

                seePresenters.StreamRevieData();
                seePresenters.events();

                revisionConnect.TimersStop();
                seePresenters.CltStop();
            }
            else
            if (clientFailed)
            {
                revisionConnect.TimersStop();
                seePresenters.CltStop();
                string mesFailed = "Failed to connect to " + inputAddress.Text;
                startBtn.Visible = true;
                stopBtn.Visible = false;
                connectText.Visible = false;
                MessageBox.Show(mesFailed, "ShareZone", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
        }        

        private void FailedConnection(object sender, EventArgs e)
        {
            seePresenters.SLengthByte();
            if (lengthByte == 0)
            {
                stopBtn_Click(sender, e);
                startBtn_Click(sender, e);     
            }            
        }

        private void startBtn_Click(object sender, EventArgs e)
        {
            seePresenters = new SeePresenters(this);

            seePresenters.ServerConnect();
            revisionFail = new TimerClass();
            revisionConnect = new TimerClass();
            revisionConnect.Timers(StartOutput, 1000);

            connectText.Visible = true;
            startBtn.Visible = false;
            stopBtn.Visible = true;
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            startBtn.Visible = true;
            stopBtn.Visible = false;
            connectText.Visible = false;

            seePresenters.CltStop();
            revisionConnect.TimersStop();
           
            if (reviewForm != null)
            {
                seePresenters.StopBtn();
                revisionFail.TimersStop();
                reviewForm.Close();
            }
            seePresenters.Dispose();
            revisionConnect.Dispose();
            revisionFail.Dispose();
        }     
    }
}

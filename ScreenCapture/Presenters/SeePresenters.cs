﻿using System;
using ScreenCapture.Views;
using ScreenCapture.Model;

namespace ScreenCapture.Presenters
{
        
    class SeePresenters : IDisposable 
    {
        ISee seeView;
        SeeModel seeModel;
        
        public void events()
        {
            seeModel.outputEvent += this.Update;
        }       

        public SeePresenters(ISee view)
        {
            seeView = view;
            seeModel = new SeeModel();
        }

        public void ServerConnect()
        {
            seeModel.GetHostAddress = seeView.IGetHostAddress;
            seeModel.StreamConnect();
        }   

        public void ConnFaild()
        {
            seeView.IClientConnected = seeModel.ClientConnected;
            seeView.IClientFailed = seeModel.ClientFailed;
        }
        
        public void StreamRevieData()
        {
            seeModel.StreamOutput();            
        }
     
        public void SLengthByte()
        {
            seeView.ILengthByte = seeModel.LengthByte;            
        }

        public void StopBtn()
        {
            seeModel.StopClient();
        }

        public void CltStop()
        {
            seeModel.StopThreadclt();
        }

        public void Update(object sender, NewEventArgs e)
        {
            seeView.OutputData(e.Bytes);
        }       

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                seeModel.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

﻿using System;
using ScreenCapture.Model;
using ScreenCapture.Views;

namespace ScreenCapture.Presenters
{
    public class ServerPresenters : IDisposable
    {
        IShare serverView;
        ShareModel shareServer;

        public ServerPresenters(IShare view)
        {
            serverView = view;
            shareServer = new ShareModel();
        }

        // Запуск сервера
        public void StartServer()
        {
            shareServer.StreamStartServer();
        }

        public void NumberClient()
        {
            serverView.IListClient = shareServer.ListClient;
        }

        // Відправка даних
        public void PresentersCaptureScreen()
        {
            shareServer.SendData = serverView.ISendData;     
            shareServer.CaptureScreen();            
        }      
        
        // Зупинка сервера
        public void IStopServer()
        {
            shareServer.StopServer();          
        }

        // Локальний іп комп'ютера
        public void ILocalAddress()
        {
            shareServer.LocalIpAddress();
            serverView.IAddressIP = shareServer.LocalIpAddress();
        }               

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                shareServer.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
